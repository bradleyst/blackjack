﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack
{
    public class Scoring
    {
        string winner;
        static public void display(List<Cards> dealerHand, List<Cards> playerHand)
        {
            Console.WriteLine("Dealer Showing:");
            Console.Write(dealerHand[1].getSymbol + " of ");
            Console.WriteLine(dealerHand[1].getSuit);

            Console.WriteLine();

            Console.WriteLine("Player Showing:");

            for (int i = 0; i < playerHand.Count; i++)
            {
                Console.Write(playerHand[i].getSymbol + " of ");
                Console.WriteLine(playerHand[i].getSuit);
            }

        }
        static public string score(List<Cards> dealerHand, List<Cards> playerHand)
        {
            int dealerScore = 0;
            int playerScore = 0;
            string winner;
            foreach (Cards card in dealerHand)
            {
                dealerScore += card.getValue;
            }
            foreach (Cards card in playerHand)
            {
                playerScore += card.getValue;
            }

            if (dealerScore > playerScore)
            {
                winner = "dealer";
            }
            else if (playerScore > dealerScore)
            {
                winner = "player";
            }
            else
            {
                winner = "push";
            }
            return winner;

        }
    }
}
