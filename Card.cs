﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack
{
    public class Cards
    {
        public int[] value = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11 };
        public string[] symbol = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "Joker", "Queen", "King", "Ace" };
        public string[] suit = { "Clubs", "Diamonds", "Hearts", "Spades" };

        public Cards() { }
        public Cards(int value, string symbol, string suit)
        {
            getValue = value;
            getSymbol = symbol;
            getSuit = suit;
        }
        public int getValue { get;}
        public string getSymbol { get; }
        public string getSuit { get; }

    }
}