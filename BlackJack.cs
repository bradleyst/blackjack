﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack
{
    class Game
    {
        static List<Cards> hit(List<Cards> playerHands, List<Cards> gameDeck) 
        {
            playerHands.Add(gameDeck[0]);
            gameDeck.RemoveAt(0);
            return playerHands;
        }
        static void Main(string[] args)
        {
            Deck startDeck = new Deck();

            String command = "";

            List<Cards> gameDeck = new List<Cards>();

            gameDeck = startDeck.createDeck();
            gameDeck = startDeck.shuffle(gameDeck);

            Player Dealer = new Player();
            Player Challenger = new Player();

            Dealer.hand = Dealer.deal(gameDeck);
            Challenger.hand = Challenger.deal(gameDeck);

            Scoring.display(Dealer.hand, Challenger.hand);
            Console.WriteLine("Do you want to hit or pass? Type hit or pass");
            do
            {
                command = Console.ReadLine();
                if (command == "hit")
                { 
                   Challenger.hand = Challenger.deal(gameDeck);
                   Scoring.display(Dealer.hand, Challenger.hand);
                   Scoring.score(Dealer.hand, Challenger.hand);
                }
            }
            while (command != "pass");
        }
    }
}
