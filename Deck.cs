﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack
{
    public class Deck : Cards
    {
        private static Random rng = new Random();
        public Deck()
        {
        }

        public List<Cards> createDeck()
        {
            List<Cards> gameDeck = new List<Cards>();
            Cards currentCard = new Cards();
            for (int i = 0; i < currentCard.suit.Length; i++) //Creating the deck of cards here. Could use an array with all the cards manually created, but wanted to have a way to store values easily
            {
                for (int j = 0; j < currentCard.symbol.Length; j++)
                {
                    Cards card = new Cards(j + 2, currentCard.symbol[j], currentCard.suit[i]);
                    gameDeck.Add(card);
                }

            }
            return gameDeck;
        }

        public List<Cards> shuffle(List<Cards> orderedDeck)
        {
            int n = orderedDeck.Count;
            List<Cards> shuffledDeck = new List<Cards>();
            while (orderedDeck.Count > 0)
            {
                n = orderedDeck.Count;
                int k = rng.Next(n);
                shuffledDeck.Add(orderedDeck[k]);
                orderedDeck.RemoveAt(k);
            }

            return shuffledDeck;
        }
    }
}
