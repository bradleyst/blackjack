﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackJack
{
    public class Player
    {
        public List<Cards> hand;
        public Player()
        {
        }
        public List<Cards> deal(List<Cards> shuffledDeck)
        {
            List<Cards> player = new List<Cards>();
            for (int i = 0; i < 2; i++)
            {
                player.Add(shuffledDeck[0]);
                shuffledDeck.RemoveAt(0);
            }
            return player;
        }
    }
}
